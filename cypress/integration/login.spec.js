describe("Login view", () => {
  it("Displays a form", () => {
    // Given the user is not logged in
    // and goes to main page
    window.sessionStorage.clear();
    cy.visit("/");

    // Then the user should see login form
    cy.get("form").within(() => {
      cy.get('[name="email"]').should("be.visible");
      cy.get('[name="password"]').should("be.visible");
      cy.get("button")
        .should("be.visible")
        .and("contain", "Zaloguj");
    });
  });

  it("User sign in with proper credentials", () => {
    cy.server();
    cy.route("POST", "/api/users/auth/sign-in").as("postSignIn");

    // Given the user sees the login form
    window.sessionStorage.clear();
    cy.visit("/");

    // When the user enters proper email
    cy.get('[name="email"]').type("some@example.com");

    // and the user enters proper password
    cy.get('[name="password"]').type("a");

    // and the user submits the form
    cy.get("button").click();

    // Then the user should signed in to the system
    cy.wait("@postSignIn");
    cy.location("pathname").should("be.equal", "/dashboard");
  });

  it("User signs in with improper credentials", () => {
    cy.server();
    cy.route("POST", "/api/users/auth/sign-in").as("postSignIn");

    // Given the user is on the login page
    window.sessionStorage.clear();
    cy.visit("/");

    // When the user enters improper email
    cy.get('[name="email"]').type("no@example.com");

    // and the user enters improper password
    cy.get('[name="password"]').type("b");

    // and the user submits the form
    cy.get("button").click();

    // Then the user sees the error message
    cy.wait("@postSignIn");
    cy.get(".ms-MessageBar-innerText").should(
      "contain",
      "Podano zły email lub hasło"
    );
  });

  it("User password has expired", () => {
    cy.server();
    cy.route({
      method: "POST",
      url: "/api/users/auth/sign-in",
      status: 409,
      response: {
        message: "Twoje hasło wymaga zmiany",
        token: "some-token"
      }
    }).as("postSignIn");

    // Given the user is on the login page
    window.sessionStorage.clear();
    cy.visit("/");

    // When the user enters proper email
    cy.get('[name="email"]').type("xyz@example.com");

    // and the user enters proper and expired password
    cy.get('[name="password"]').type("x");

    // and the user submits the form
    cy.get("button").click();

    // Then the user sees the error message
    cy.wait("@postSignIn");
    cy.get(".ms-MessageBar-innerText").should(
      "contain",
      "Twoje hasło wymaga zmiany"
    );
  });

  it("User submits invalid form", () => {
    // Given the user is on the login page
    window.sessionStorage.clear();
    cy.visit("/");

    // When the user submits the login form
    cy.get('form button[type="submit"]').click();

    // Then the user should see the form error message
    cy.get("form").then($form => {
      expect($form[0].checkValidity()).to.be.false;
    });

    // and the user should be on sign in form page
    cy.location("pathname").should("be.equal", "/sign-in");
  });
});
